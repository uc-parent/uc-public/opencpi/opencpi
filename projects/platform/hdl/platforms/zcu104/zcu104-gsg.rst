.. zcu104 Getting Started Guide Documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _zcu104-gsg:

.. |trade| unicode:: U+2122
   :ltrim:

.. |reg| unicode:: U+00AE
   :ltrim:


OpenCPI Xilinx ZCU104 Getting Started Guide
===========================================

.. This is the main file for the ZCU104 Getting Started Guide.
   This file contains information that is specific to the ZCU104.

.. The directives below include the files in hdl/platforms/include/ into this document.
   These files contain information that applies to all ZCU platforms. They use substitution
   strings to indicate where platform-specific values should be used when the file is included
   into a document for a specific ZCU platform. The substitution strings defined in this file
   match the strings used in the common include files. In this file, They are set to the
   values for the ZCU104 and are applied when the generic files are used in this document.


Document Revision History
-------------------------

.. csv-table:: OpenCPI ZCU104 Getting Started Guide: Revision History
   :header: "OpenCPI Version", "Description of Change", "Date"
   :widths: 10,30,10
   :class: tight-table

   "v2.1", "Initial Release", "2/2025"


.. ocpi_documentation_include:: ../include/zcu_overview.inc
				
   |device_name|: ZCU104
   |platform_name|: zcu104
   |path_to_figures|: ../include/zcu104_figures/
   |device_type|: Multiprocessor System-on-Chip (MPSoC)
   |product_brief|: product brief at https://www.xilinx.com/products/boards-and-kits/zcu104.html

.. ocpi_documentation_include:: ../include/zcu_hw_setup.inc

   |device_name|: ZCU104
   |platform_name|: zcu104
   |path_to_figures|: ../include/zcu104_figures/
   |sd_boot_pins|: SW6[4:1]
   |user_guide|: section "MPSoC Device Configuration" in the ZCU104 User Guide at https://docs.amd.com/v/u/en-US/ug1267-zcu104-eval-bd

  
.. ocpi_documentation_include:: ../include/zcu_dc_setup.inc

   |device_name|: ZCU104
   |platform_name|: zcu104
   |path_to_figures|: ../include/zcu104_figures/
   |dc_slot_config|: one FPGA Mezzanine Card (FMC) High Pin Count (HPC) slot
   
.. ocpi_documentation_include:: ../include/zcu_sfw_setup.inc
				
   |device_name|: ZCU104
   |platform_name|: zcu104
   |prompt_name|: zcu104
   |path_to_figures|: ../include/zcu104_figures/
   |dtype_abbrv|: MPSoC
   |sd_slot_location|: below the pushbuttons
   |uart_location|: on the top side
   |uart_label|: USB/JTAG/UART on the board and **USB UART** in the vendor documentation

