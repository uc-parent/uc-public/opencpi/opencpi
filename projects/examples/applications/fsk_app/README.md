## FSK_APP

The fsk_app example applications can be run independently using the ‘ocpirun’ CLI command or using the
OpenCPI ACI application. Each application should be generic enough to run on any supported OpenCPI host
and hardware platform. Currently, testing was conducted on ubuntu22_04, e31x, zed, and plutosdr platforms. There
are three application modes that demonstrate the transmission and reception of a data file. The
modes are ‘txrx’ which provides a RF loopback example on a single platform, a ‘tx’ only mode to send data,
and a ‘rx’ only mode to receive data. The ‘tx’ and ‘rx’ modes may be used to transfer a data file between
two platforms.

The applications implement a Frequency Shift Keying (FSK) modulation scheme using OpenCPI components found
in the SDR components library of the OpenCPI git repository (https://gitlab.com/opencpi/comp/ocpi.comp.sdr).
This library should be cloned to the ~/opencpi/projects/ folder. The components should then be built for
the intended HDL and RCC platforms.

At this time, the only tested OpenCPI hardware platforms are:
        e31x
        zed
        plutosdr
        zcu104

The HDL assemblies supporting the fsk_app are comprised of a “all” RCC and a “mostly” HDL assembly. They
are located in:
```sh
        ~/opencpi/projects/examples/hdl/assemblies/fsk_app/
```
There are six assemblies to support each mode (txrx, tx, rx) and model (hdl, rcc). These assemblies should
be built for the intended SDR platform and desired model. The "all" RCC implements the FSK algorithm using
only RCC workers but still requires HDL components to support the digital radio controller (DRC)
functionality to include rate conversion and complex mixing and interface with the platforms RF elements
(e.g. ad9361). The mostly HDL application relies on RCC components primarily for reading and writing to/from
data files.

It is typically desirable to utilize as much of the HDL resources as possible. The RCC assemblies are
intended to provide examples where a particular SDR platform, such as the plutosdr, may not provide 
these resources. The examples are also intended to show the extreme cases using all RCC to mostly HDL workers.
A new user should explore development of their own assemblies moving some workers from the RCC only case to HDL.
The mostly HDL application relies on RCC components primarily for reading and writing to/from data files. 
The plutosdr platform only supports the "all" RCC assemblies due to lack of resources of the Xilinx 7010 FPGA.

FSK_APP Guide
---
The FSK Application guide can be found in the opencpi gitlab.io documenation site:
```sh
https://opencpi.gitlab.io/releases/v2.4.8/docs/OpenCPI_FSK_Application_Guide.pdf
```
Register Project
---

Before using examples withing the examples project, the project must be registered with the OpenCPI framework.
Execute the following: 
```sh	
        $ cd ~/opencpi/projects/examples
        $ ocpidev register project
```
You can verify by viewing all registered projects:
```sh
        $ ocpidev show registry
```
        Project registry is located at: ~/opencpi/project-registry

-------------------------------------------------------------------------------------------
| Project Package-ID | Path to Project                                     | Valid/Exists |
| ------------------ | --------------------------------------------------- | ------------ |
| ocpi.examples      | ~/opencpi/projects/examples                         | True         |
| ocpi.osp.e3xx      | ~/opencpi/projects/osps/ocpi.osp.e3xx               | True         |
| ocpi.core          | ~/opencpi/projects/core                             | True         |
| ocpi.comp.sdr      | ~/opencpi/projects/ocpi.comp.sdr                    | True         |
| ocpi.tutorial      | ~/opencpi/projects/tutorial                         | True         |
| ocpi.platform      | ~/opencpi/projects/platform                         | True         |
| ocpi.osp.plutosdr  | ~/opencpi/projects/osps/ocpi.osp.plutosdr           | True         |
| ocpi.assets_ts     | ~/opencpi/projects/assets_ts                        | True         |
| ocpi.assets        | ~/opencpi/projects/assets                           | True         |
-------------------------------------------------------------------------------------------

OpenCPI Component Library
---

NOTE: It is assumed the user has successfully installed the desired platforms(e.g. e31x, zed, zcu104, plutosdr, 
xilinx24_1_aarch32, adi_plutosdr0_38) prior to building primitives, workers, or assemblies described in these 
instructions.

To build the fsk_app application assemblies, the OpenCPI Component Library must be cloned into and registered
with the OpenCPI Framework. Once cloned, the Component library primitives and components must be built for the
desired platforms. The example below shows these steps for Ubuntu 22.04 RCC platform and Ettus E31X HDL platform.
```sh
        ~/opencpi/projects$ git clone https://gitlab.com/opencpi/comp/ocpi.comp.sdr.git
        ~/opencpi/projects/ocpi.comp.sdr$ ocpidev register project
        ~/opencpi/projects/ocpi.comp.sdr$ ocpidev build --rcc-platform ubunu22_04
        ~/opencpi/projects/ocpi.comp.sdr/hdl/primitives$ ocpidev build --hdl-platform e31x
```
Once primitives are built for the HDL platform, assembly workers will be built using the '--workers-as-needed' option
when building the application assembly.

There are two custom components that support the fsk_app that must be built for the RCC platform.
```sh
        ~/opencpi/projects/examples/components/build_packet_uc.rcc$ ocpidev build --rcc-platform ubuntu22_04
        ~/opencpi/projects/examples/components/discontinuity_gate_b.rcc$ ocpidev build --rcc-platform ubuntu22_04
```
Platform Digital Radio Controller(DRC)
---

Each hardware platform has a digital radio controller (DRC) to configure RF resources such as tuning frequency, sample rates, filters, gains, etc. The DRCs are considered RCC workers that must be built for the host platform such as Ubuntu22_04, xilinx24_1_aarch32, adi_plutosdr0_38 OS, etc.

Once the desired SDR platform has been installed, then the DRC must be built for the desired platform that will host
the execution of the application. Because the comp.sdr project is not currently part of the initial install of opencpi 
or the e31x and plutosdr, the DRC workers for each platform are temporarily placed in the examples project hdl/devices 
folder. Prior to building each DRC, the comp.sdr project must be installed. 

Below are the locations of the DRC for each of the currently supported SDR platforms:
	
        ~/opencpi/projects/examples/hdl/devices/drc_fmcomms.rcc/
        ~/opencpi/projects/examples/hdl/devices/drc_e31x.rcc/
        ~/opencpi/projects/examples/hdl/devices/drc_plutosdr.rcc/

If the application will be executed remotely from and Ubuntu 22.04 platform, move to the respective DRC folder and build using the command:
```sh
        ocpidev build --rcc-platform ubuntu22_04
```
If the applications are to be executed locally, for example on the ARM processor of the e31x, zcu104, or zed platforms then build using the following command:
```sh
       ocpidev build --rcc-platform xilinx24_1_aarch32
```
For remote execution on the Pluto SDR platform, then execute the following to build the DRC:
```sh	
      ocpidev build --rcc-platform adi_plutosdr0_38
```
NOTE: If this application will be run locally on the hardware platform, then all artifacts and files required to execute applications must be moved to the appropriate folders of the platform. See other OpenCPI documention for specific platform for local execution. 

FSK_APP assemblies
---

The fsk_app assemblies are located in the ~/opencpi/projects/examples/hdl/assemblies/fsk_app/ directory.
Each desired mode [txrx, tx, rx] and model [hdl, rcc] must be built for the desired platform.
For example:
```sh
        ~/opencpi/projects/examples/hdl/assemblies/fsk_app/fsk_app_txrx_hdl_assy$ ocpidev build --hdl-platform e31x
         --workers-as-needed
```
Due to RESOURCE UTILIZATION contraints, the 'txrx', 'tx', and 'rx' mode assemblies for the 'hdl' model are not supported for the plutosdr platform.

Application Execution (using ocpirun):
---

Before execution, it is assumed that the user installed the appropriate platforms, built the required
assemblies, correctly set the OCPI_LIBRARY_PATH, registered required projects, and configured the platform according to expected use mode (remote server, standalone, or network).

Each of the applications may be executed using the ‘ocpirun’ cli command. However, because some of the
component workers (e.g.‘fir_filter_scaled_s’ and ‘moving_average_filter_b’) implement properties with different
names based on model (hdl, rcc), the application may not be run without providing the properties using the
property (-p) option. Below shows examples how to execute each of the application modes using the ‘ocpirun’
command. The examples show execution using the ‘remote server’ mode from an Ubuntu 22.04 host.

NOTE: If running in Rocky 8 or 9, then you may need to turn off or configure firewall.
```sh
        $ sudo systemctl stop firewalld.service
```
[ RCC model ]
---
**NOTE**

The FSK application does not perform any form of carrier recovery to compensate for frequency offset. If using the TX and RX apps to send data between two different platforms, you must first determine the offset and apply to either the 'tuning_freq_mhz' or 'offset_freq_mhz' values of the DRC configuration within the application so actual TX carrier and RX carrier closely match.

The RCC model does have limitation on the achieveable sample rates due to host interface and processing.  To achieve higher rates, consider the HDL model.

---
        ~/opencpi/projects/examples/applications/fsk_app$ ocpirun -P file_read=ubuntu22_04 -m unpack_bits=rcc
                -p mov_avg=moving_average_size=10 apps/fsk_app_txrx.xml

        ~/opencpi/projects/examples/applications/fsk_app$ ocpirun -P file_read=ubuntu22_04 -m unpack_bits=rcc -t 20
                apps/fsk_app_tx.xml

        ~/opencpi/projects/examples/applications/fsk_app$ ocpirun -P file_write=ubuntu22_04 -m fm_demod=rcc
                -p mov_avg=moving_average_size=10 apps/fsk_app_rx.xml

[ HDL model ]
---

**NOTE**

The FSK application does not perform any form of carrier recovery to compensate for frequency offset. If using the TX and RX ACI apps to send data between two different platforms, you must first determine the offset. The correction may be applied by modifying the specified carrier using the --tx-freq or --rx-freq options or the --tx-offset or --rx-offset options to compensate frequency so that the actual TX carrier and RX carrier closely match.

---
        ~/opencpi/projects/examples/applications/fsk_app$ ocpirun -P file_read=ubuntu22_04 -m unpack_bits=hdl
                -p mov_avg=maximum_moving_average_size=10 -p tx_filter=number_of_multipliers=8
                -p rx_filter=number_of_multipliers=8 apps/fsk_app_txrx.xml

        ~/opencpi/projects/examples/applications/fsk_app$ ocpirun -P file_read=ubuntu22_04 -m unpack_bits=hdl
                -p tx_filter=number_of_multipliers=8 -t 20 apps/fsk_app_tx.xml

        ~/opencpi/projects/examples/applications/fsk_app$ ocpirun -P file_write=ubuntu22_04 -m fm_demod=hdl
                -p rx_filter=number_of_multipliers=8 -p mov_avg=maximum_moving_average_size=10
                apps/fsk_app_rx.xml

NOTE 1: DRC parameters such as tuning frequency, sample rates, and bandwidth may be changed directly by
modifying the application file.

NOTE 2: The ‘tx’ only application is run with the timeout (-t) option because there currently isn’t an
implementation that provides notification of the ‘done’ state. Based on rate, this time must be adequately
set to provide time to move bytes from file and transmit.

Application Execution (using ACI):
---

As described in the standalone execution above, it is assumed that all necessary installations and
configuration are in place to execute.

The OpenCPI Application Control Interface (ACI) provides an ability to control OpenCPI application execution
through C++ coding. The current fsk_app.cc application selects the desired application to be executed based on
user provided option --mode. It also automates the selection of worker properties based on desired model
(hdl or rcc). The fsk_app should be executed from the fsk_app/ folder. Once the application is built, the
executable is located in the target platform folder. This example shows the executable application as built for the
‘ubuntu22_04’ RCC platform running the 'txrx' XML Application for the rcc and hdl models.

[ RCC model ]
```sh
        ~/opencpi/projects/examples/applications/fsk_app$ ./target-ubuntu22_04/fsk_app –-mode txrx –-rcc-platform ubuntu22_04
                --hdl-platform e31x --model rcc
```
[ HDL model ]
```sh
        ~/opencpi/projects/examples/applications/fsk_app$ ./target-ubuntu22_04/fsk_app –-mode txrx –-rcc-platform ubuntu22_04
                --hdl-platform e31x --model hdl
```
Execution with the --help option will display the available options along with a brief description and show
the default values.
```sh
        ~/opencpi/projects/examples/applications/fsk_app$ ./target-ubuntu22_04/fsk_app –-help
```
```sh
Options:
 --help                Display options
 --display             Show final configuration properties
 --verbose             Show ocpirun debug.
 --mode                Values = {txrx, tx, rx}, [default=txrx]
 --rcc-platform        Defines which OS platform to run RCC components, [default = ubuntu22_04]
 --hdl-platform        Defines which sdr platform to run hdl components, [default = e31x]
 --model               Select authoring model {rcc, hdl}, [default = rcc]
 --tx-freq             Transmit carrier frequency (MHz), [default = 2450.0]
 --rx-freq             Receive carrier frequency (MHz),  [default = 2450.0]
 --tx-offset           Transmit frequency offset (MHz), [default = 0.0]
 --rx-offset           Receive frequency offset (MHz),  [default = 0.0]
 --tx-filename         Name of file to send, [default=spurs.bmp]
 --rx-filename         Name of file to save received data, [default=opencpi.jpg]
 --tx-rate             Transmit sample rate (Msps), [default = 0.25]
 --rx-rate             Receive sample rate (Msps),  [default = 0.25]
 --tx-bandwidth        Transmit 3dB bandwidth (MHz),[default = 0.25]
 --rx-bandwidth        Receive 3dB bandwidth (MHz), [default = 0.25]
 --tx-gain             Transmit gain = gain_dB (dB), [default = 0.0]
 --rx-gain             Receive gain =  {'auto'} (dB)}, [default = auto]
 --timeout             Duration to run application (seconds), [default = -1] (no timeout)
```
