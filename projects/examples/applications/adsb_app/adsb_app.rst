.. adsb_app documentation


.. _adsb_app-application:

ADS-B Application (``adsb_app``)
================================
The ADS-B (Automatic Dependent Surveillance-Broadcast) application is a receiver only application designed to process Mode S packets transmitted from commercial and military aircraft.

Description
-----------
The Automatic Dependent Surveillance-Broadcast (ADS-B) protocol is an aviation surveillance technology used by aircraft to broadcast its position and other data enabling it to be tracked augmenting traditional radar surveillance capabilities. The signal is periodically transmitted at a data rate of 1 Msps on carrier frequency 1090 MHz using pulse position modulation (PPM). This application is specifically designed to receive, detect, demodulate, and perform error checking of Mode S Reply signals. Mode S supports both a 56-bit and 112-bit data block but this application currently only supports the 112-bit signal.

The transmitted Mode S signal consists of a preamble, the data, and cyclic redundancy check (CRC) value as shown in :numref:`adsb_packet-diagram`

.. _adsb_packet-diagram:

.. figure:: fig/adsb_packet.svg
   :alt: Skeleton alternative text.
   :align: center

   ADS-B Mode S packet format.

This OpenCPI application does not directly parse the Mode S data which includes aircraft identity, position, elevation, velocity, etc. This application performs all signal processing using FPGA based HDL components and then sends those data packets determined to be error free to a third-party application to parse and display each identified aircraft's position on a map overlay. We specifically demonstrate the application using the open source Tar1090 application but the data may be sent to any available ADS-B tracking service.

The application is executed using the OpenCPI Application Control Interface (ACI). The use of ACI is required to establish a connection to the Tar1090 service to send the processed ADS-B data packets extracted from the output port of the CRC Validator component.

The adsb_app.xml application is composed with the following OpenCPI components shown in :numref:`adsb_ocpi_processing_path-diagram`. Details regarding library location and specified property values may be observed by viewing the application directly. The names of each are abbreviated for simplicity in explaining functionality. Those components designed specifically to support this ADS-B application are found in the ‘examples’ project component library. The other components can be found in the ‘ocpi.comp.sdr’ project component libraries.

.. _adsb_ocpi_processing_path-diagram:

.. figure:: fig/adsb_receiver_processing_path.svg
   :alt: Skeleton alternative text.
   :align: center

   ADS-B application RX HDL processing components.

A single OpenCPI Application and HDL Assembly may be ported to support OpenCPI compliant platforms. Currently, the application has been tested for the following OpenCPI platforms:

 * Ettus E310
 * Analog Device ADALM-Pluto
 * Digilent ZedBoard with Analog Device FMCOMMS2 daughter board
 
Note: All components for this application are implemented as HDL workers.

Digital Radio Controller
~~~~~~~~~~~~~~~~~~~~~~~~
The DRC component drives the specific OpenCPI platform. The DRC for this application is set as receive only and specifies a carrier frequency of 1090 MHz and sampling rate of 4 MSps to support ADB-S processing. This rate provides 4 samples per symbol for detection and demodulation.

Complex Multipier
~~~~~~~~~~~~~~~~~
The complex multiplier is used to scale the analog-to-digital converter (ADC) real and quadrature data outputs from signed 2^11 to signed 2^15 format for processing. The complex constant generates the scaling factor.

Magnitude Squared
~~~~~~~~~~~~~~~~~
Generates a real value output of signal with phase element of received signal removed. Removing phase compensates for carrier and phase offset.

Moving Average
~~~~~~~~~~~~~~
The moving average component generates a reference threshold to be used by the ADSB preamble detector. The average value is determined by the number of samples of the preamble. Since the preamble consists of 8 symbols and there are 4 samples per symbol, the averaging value used for this application is 32.

ADS-B Preamble Detector
~~~~~~~~~~~~~~~~~~~~~~~
The preamble detector generates a stream of pulses by comparing the input samples to a threshold. The average input sample is used as the referenced threshold value. If the input sample is greater than the corresponding threshold, a pulse is generated, else a zero is generated.

The stream of pulses is then matched to the ADS-B preamble pattern as shown in :numref:`adsb_preamble-diagram`

.. _adsb_preamble-diagram:

.. figure:: fig/adsb_preamble.svg
   :alt: Skeleton alternative text.
   :align: center

   ADS-B Mode S Preamble.

When a match is detected, the data block and CRC symbol samples are passed from the input port to the output port. The preamble is stripped and not passed to output port. A total of 448 samples which represent 112 ADS-B symbols is sent to the output port. The first data symbol sample is aligned with a start-of-message (SOM) flag.

PPM Demodulation
~~~~~~~~~~~~~~~~
Pulse Position Modulation (PPM) is a technique that modulates a carrier signal on and off over a duration of symbol period. If the pulse position is "on" the first half of the symbol, then the bit value is interpreted to be ‘1’, else ‘0’ as shown in :numref:`adsb_ppm_symbols-diagram`

.. _adsb_ppm_symbols-diagram:

.. figure:: fig/ppm_modulation.svg
   :alt: Skeleton alternative text.
   :align: center

   Pulse Position Modulation (PPM) symbols.

The PPM demodulation component receives the detected ADS-B symbol samples on its input port aligned with start of message (SOM) flag for a 448-sample message. Each symbol consists of 4 samples. Each set of 4-samples is used to derive a bit value which is then sent to as a boolean stream to the output port.

Message Length (112)
~~~~~~~~~~~~~~~~~~~~
The message length adapter component is used to maintain the structure of the 112 boolean data bits associated with demodulated ADS-B data block. This is required because at this time, the PPM demodulation component does not support the standard OpenCPI message structure using start and end of message (SOM and EOM) flags.

Pack Data
~~~~~~~~~
This component packs the boolean input bits into byte format and passes the byte to output port in unsigned character samples.

Message Length (14)
~~~~~~~~~~~~~~~~~~~
This message length adapter is used to maintain ADS-B packet alignment to 14-byte output samples.

CRC Validator
~~~~~~~~~~~~~
The CRC validator verifies that no errors are present within the received ADS-B data using polynomial division. A divisor known as the CRC key is defined by the ADS-B protocol for Mode S as the 25-bit value 0x1FF409. The divisor is divided into the data block comprising both the data and CRC. If the division produces a remainder value of all zeros, then the data is deemed to be error free.

Hardware Portability
--------------------
This application has been tested and verified to execute on the following platforms:

 * e31x
 * plutosdr
 * zed
 * ubuntu20_04

Execution
---------
Please refer to the ADSB application README.md file in the project's git repository for instructions to build and execute the ADS-B Application.

A more detailed guide and instructions for FSK application execution maybe found in the "OpenCPI FSK Application Guide" found at the opencpi.gitlab.io/releases OpenCPI Documentation site. It is highly recommended for users new to OpenCPI.   

Troubleshooting
---------------
None at this time.

Worker Artifacts
----------------
The application currently supports an all HDL component assembly. The assembly is located in the following directory:
	
      ~/opencpi/projects/examples/hdl/assemblies/adsb_app/adsb_app_rx_hdl_assy/

Three platform container files are available to build the assembly for the ‘zed’, ‘e31x’, and ‘plutosdr’ platforms and located in the assembly directory.

