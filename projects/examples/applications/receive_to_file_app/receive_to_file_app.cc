//
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//

#include <getopt.h>
#include <iostream>
#include <vector>
#include "OcpiApi.hh"
#include "receive_to_file_app.hh"

namespace OA = OCPI::API;

// *****************************************************************************
// Functions to get and set application DRC properties.
// *****************************************************************************

std::string app_config_value;

// Tuning Frequency
std::string get_tuning_frequency(OA::Application &app, int32_t channel) {
  app.getProperty("drc.configurations", app_config_value,
                  {0, "channels", channel, "tuning_freq_MHz"});
  return app_config_value;
}
void set_tuning_frequency(OA::Property &configs, double frequency,
                          int32_t channel) {
  configs.setValue(frequency, {0, "channels", channel, "tuning_freq_MHz"});
  return;
}
// Frequency Offset
std::string get_offset_frequency(OA::Application &app, int32_t channel) {
  app.getProperty("drc.configurations", app_config_value,
                  {0, "channels", channel, "offset_freq_MHz"});
  return app_config_value;
}
void set_offset_frequency(OA::Property &configs, double offset,
                          int32_t channel) {
  configs.setValue(offset, {0, "channels", channel, "offset_freq_MHz"});
  return;
}

// Sampling Rate
std::string get_sample_rate(OA::Application &app, int32_t channel) {
  app.getProperty("drc.configurations", app_config_value,
                  {0, "channels", channel, "sampling_rate_Msps"});
  return app_config_value;
}
void set_sampling_rate(OA::Property &configs, double rate, int32_t channel) {
  configs.setValue(rate, {0, "channels", channel, "sampling_rate_Msps"});
  return;
}
// Bandwidth
std::string get_bandwith(OA::Application &app, int32_t channel) {
  app.getProperty("drc.configurations", app_config_value,
                  {0, "channels", channel, "bandwidth_3dB_MHz"});
  return app_config_value;
}
void set_bandwidth(OA::Property &configs, double bandwidth, int32_t channel) {
  configs.setValue(bandwidth, {0, "channels", channel, "bandwidth_3dB_MHz"});
  return;
}
// Gain
std::string get_gain(OA::Application &app, int32_t channel) {
  app.getProperty("drc.configurations", app_config_value,
                  {0, "channels", channel, "gain_mode"});
  return app_config_value;
}
void set_gain(OA::Property &configs, std::string gain_mode, int32_t gain,
              int32_t channel) {
  configs.setValue(gain_mode, {0, "channels", channel, "gain_mode"});
  if (gain_mode == "manual")
    configs.setValue(gain, {0, "channels", channel, "gain_dB"});
  return;
}

int32_t main(int32_t argc, char **argv) {
  // ***************************************************************************
  //  Process application options.
  // ***************************************************************************
  int32_t opt;
  char *arg_long = nullptr;
  int32_t option_index;

  // Process option, if not defined, then application will use default values
  while ((opt = getopt_long(argc, argv, "h", long_options, &option_index)) !=
         -1) {
    switch (opt) {
      case 0: {
        arg_long = optarg;
        if (strcmp(long_options[option_index].name, "mode") == 0) {
          mode = arg_long;
          if (mode == "rx") {
          } else {
            printf("\nInvalid 'mode' selected.\n");
            usage();
          }
        }
        if (strcmp(long_options[option_index].name, "rcc-platform") == 0) {
          rcc_platform = arg_long;
          bool is_valid_rcc_platform = check_rcc_platform(rcc_platform);
          if (!is_valid_rcc_platform) {
            printf("\nInvalid RCC platform specified.\n");
            printf("Valid RCC platforms are: { ");
            for (int32_t i = 0; i < NUM_RCC_PLATFORMS - 1; i++)
              std::cout << RCC_PLATFORM[i] << ", ";
            std::cout << RCC_PLATFORM[NUM_RCC_PLATFORMS - 1] << " }"
                      << std::endl;
            exit(1);
          }
        }
        if (strcmp(long_options[option_index].name, "hdl-platform") == 0) {
          hdl_platform = arg_long;
          bool is_valid_hdl_platform = check_hdl_platform(hdl_platform);
          if (!is_valid_hdl_platform) {
            printf("\nInvalid HDL platform specified.\n");
            printf("Valid HDL platforms are: { ");
            for (int32_t i = 0; i < NUM_HDL_PLATFORMS - 1; i++)
              std::cout << HDL_PLATFORM[i] << ", ";
            std::cout << HDL_PLATFORM[NUM_HDL_PLATFORMS - 1] << " }"
                      << std::endl;
            exit(1);
          }
        }
        if (strcmp(long_options[option_index].name, "model") == 0) {
          model = arg_long;
          if (model == "rcc" || model == "hdl") {
          } else {
            printf("\n%s is not a valid 'model'.\n", model.c_str());
            usage();
          }
        }
        if (strcmp(long_options[option_index].name, "rx-freq") == 0) {
          rx_frequency = atof(arg_long);
        }
        if (strcmp(long_options[option_index].name, "rx-offset") == 0) {
          rx_offset = atof(arg_long);
        }
        if (strcmp(long_options[option_index].name, "rx-filename") == 0) {
          rx_filename = arg_long;
        }
        if (strcmp(long_options[option_index].name, "file-type") == 0) {
          file_type = arg_long;
        }
        if (strcmp(long_options[option_index].name, "rx-rate") == 0) {
          rx_sample_rate = atof(arg_long);
        }
        if (strcmp(long_options[option_index].name, "rx-bandwidth") == 0) {
          rx_bandwidth = atof(arg_long);
        }
        if (strcmp(long_options[option_index].name, "rx-gain") == 0) {
          rx_gain = atof(arg_long);
        }
        if (strcmp(long_options[option_index].name, "rx-gain-mode") == 0) {
          rx_gain_mode = arg_long;
        }
        if (strcmp(long_options[option_index].name, "timeout") == 0) {
          timeout = atoi(arg_long);
          // Convert to microseconds
          timeout = timeout * 1e6;
        }
        break;
      }
      // Process option help request.  Display option usage.
      case 'h': {
        usage();
        break;
      }
      case '?': {
        std::cout << "\nGot unknown option." << std::endl;
        usage();
        break;
      }
      default: {
        std::cout << "\nGot unknown parse returns: " << opt << std::endl;
        usage();
      }
    }
  }

  // If display_flag set, display final properties to be used by application
  if (display_flag) {
    if (mode == "rx") display_rx_properties();
  }

  // ***************************************************************************
  // Set OpenCPI application options and select application XML file based on
  // defined 'mode' value {rx}.
  // ***************************************************************************

  std::string app_path = "apps/";
  std::string app_file;

  // Property Value class defined as vector to allow variable size of parameters
  // set for each 'mode'.
  std::vector<OA::PValue> params_vec;

  std::string rcc_platform_rx = "file_write=" + rcc_platform;
  rx_filename = "file_write=filename=" + rx_filename;

  if (mode == "rx") {
    if(file_type == "sc16"){
       app_file = "receive_to_file_app_xs.xml";
    }
    else if (file_type == "fc32"){
       app_file = "receive_to_file_app_xf.xml";
    }
    else if (file_type == "short"){
       app_file = "receive_to_file_app_s.xml";
    }
    else if (file_type == "float"){
       app_file = "receive_to_file_app_f.xml";
    }
    else{
       printf("\nUnspecified file type of %s not supported\n.",file_type.c_str());
       usage();
       return(0);
    }
  
    params_vec.push_back(OA::PVBool("verbose", verbose_flag));
    params_vec.push_back(OA::PVString("container", rcc_platform_rx.c_str()));
    params_vec.push_back(OA::PVString("property", rx_filename.c_str()));
    params_vec.push_back(OA::PVEnd);
  }

  // ***************************************************************************
  // Initiate application and set DRC configurations
  // ***************************************************************************

  // Convert param vector back to PValue class
  OA::PValue *params = &params_vec[0];

  try {
    OA::Application app(app_path + app_file, params);

    app.initialize();
    OA::Property configs(app, "drc", "configurations");

    // Set mode 'rx' drc configuration
    if (mode == "rx") {
      set_tuning_frequency(configs, rx_frequency, 0);
      set_offset_frequency(configs, -1*rx_offset, 0);
      set_sampling_rate(configs, rx_sample_rate, 0);
      set_bandwidth(configs, rx_bandwidth /*rx_bandwidth*/, 0);
      set_gain(configs, rx_gain_mode, rx_gain, 0);  // RX currently always 'auto'
    }

    app.start();  // execution is started

    app.wait(timeout);  // wait until app is "done" or specified timeout
    app.finish();       // do end-of-run processing like dump properties
    // app.stop();

  } catch (std::string &e) {
    std::cerr << "app failed: " << e << std::endl;
    return 1;
  }
  return 0;
}
