.. crc_validator_uc documentation


.. _crc_validator_uc:


CRC Validation (``crc_validator_uc``)
=====================================
CRC (Cyclic Redundancy Check) validation.

Design
------
The Cyclic Redundancy Check (CRC) is an error-detecting code derived from polynomial division.  A common divisor, known as the CRC key,  is selected to generate the code by the sender and used by the receiver to determine if a data packet arrived error free.  The remainder of the polynomial division is the check value.   

The user may specify a CRC Key size and value but for the ADS-B application, the CRC key consists of 25 bits :eq:`adsb_crc_key-equation` resulting in a 24-bit CRC value.

.. math::
   :label: adsb_crc_key-equation

   crcKey = 0x1FFF409

The resulting CRC value is sent along with the data in the packet as shown in :numref:`adsb_packet-diagram`.

.. _adsb_packet-diagram:

.. figure:: adsb_packet_with_crc.svg
   :alt: Skeleton alternative text.
   :align: center

   Data packet with CRC.

At the receiver, the long division is repeated using the same common divisor CRC key but keeps the tail bit check value in the dividend.  If the derived remainder is zero, then it is determined that there are no error bits.

Once the component determines that there are no error bits in the received packet, the component will send the packet unmodified with the CRC to the output port.  If errors are detected, the full packet will be discarded.    

The following examples are simple illustrations of the derivation of the check value and use in error detection.

Derive CRC
~~~~~~~~~~
.. _derive_crc-diagram:

.. figure:: derive_crc.svg
   :alt: Skeleton alternative text.
   :align: center

   Derive CRC using polynomial division.


Check Errors
~~~~~~~~~~~~

.. _check_error-diagram:

.. figure:: check_error.svg
   :alt: Skeleton alternative text.
   :align: center

   Check CRC using polynomial division.



Interface
---------
.. literalinclude:: ../specs/crc_validator_uc-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
This component does not support opcode processing or forwarding other than samples.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../crc_validator_uc.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``crc_validator_uc`` are:

 * Input and output packet size is currently specified as a parameter at run-time.

Testing
-------
.. ocpi_documentation_test_platforms::

.. Removed ocpi_documentation_test_result_summary directive until it is functional
