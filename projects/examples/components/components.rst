.. Components library index page


Components Component Library
============================
The components in the examples project are explicitly developed to support the example
applications in this project. They typically are not components that would be used for
general purpose. If components are developed that could support common use, consideration
should be made to place component in the comp.sdr project.

.. toctree::
   :maxdepth: 1
   :glob:
   :caption: Components components

   *.comp/*-index
   *.comp/*-comp

..
   "*.comp/*-index" is for backward compatibility with older component document naming schemes.
