#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', 'ocpi.examples', 'components', 'include'))

from q15 import q15
import ocpi_message


if __name__ == '__main__':
    length = int(os.environ.get("OCPI_TEST_length"))
    input_message = ocpi_message.from_file(sys.argv[-1], 'real', messages_in_file=True)
    output_message = ocpi_message.from_file(sys.argv[-2], 'real', messages_in_file=True)

    
    input_mod = [y for x in input_message for y in x]
    input_mod = input_mod[:-length+1]
    output_mod = [y for x in output_message for y in x]
    if input_mod == output_mod:
        exit(0)
    exit(-1)
