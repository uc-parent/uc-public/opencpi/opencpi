#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', 'ocpi.sdr.assets', 'components', 'include'))

import ocpi_message
from q15 import q15

sps = int(os.environ.get("OCPI_TEST_samp_per_symbol"))
num_symbols = int(os.environ.get("OCPI_TEST_num_of_symbols"))
output_message = ocpi_message.from_file(sys.argv[-3], 'real', messages_in_file=True)

if len(output_message) != 8:
    print(len(output_message))
    print("Incorrect Number of Messages")
    exit(-1)
else:
    for message in output_message:
        if message.length() != num_symbols * sps:
            print("Bad Message Output Length")
            exit(-1)
