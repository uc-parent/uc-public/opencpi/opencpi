#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import sys
import os

try:
    import matplotlib.pyplot as plt
except (ImportError, RuntimeError):
    print("Matplotlib is not installed")
    exit(1)

sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', 'ocpi.examples', 'components', 'include'))

try:
    from q15 import q15
    import ocpi_message
    from signalgen import view, signals
    import cmath
except (ImportError, RuntimeError):
    print("Could not find OCPI Libraries!")
    exit(1)

def read_in_real(filename):
    input_messages = ocpi_message.from_file(filename, 'real', messages_in_file=False)
    cmplx_in = []
    for message in input_messages:
        cmplx_in += message
    cmplx_in = [num for num in cmplx_in]
    return cmplx_in

def read_out_real(filename):
    output_messages = ocpi_message.from_file(filename, 'real', messages_in_file=False)
    cmplx_out = []
    for message in output_messages:
        cmplx_out += message
    cmplx_out = [num for num in cmplx_out]
    return cmplx_out

if __name__ == '__main__':
    # input_messages = ocpi_message.from_file(sys.argv[-1], 'real', messages_in_file=True)
    # output_messages = ocpi_message.from_file(sys.argv[-2], 'real', messages_in_file=False)

    print(sys.argv)

    # in_avg = read_in_real(sys.argv[-1])

    in_real = read_in_real(sys.argv[-2])

    # plot the input signal
    view.figure()
    view.time_plot(in_real, linelabel="Input Real", title="input_sample")
    view.scatter_plot(in_real)
    view.legend()

    out_real = read_out_real(sys.argv[-3])

    view.figure()
    view.time_plot(out_real, linelabel="output")
    view.scatter_plot(out_real)
    view.legend()

    view.show()
