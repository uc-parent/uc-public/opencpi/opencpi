#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import numpy as np
try:
    sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', 'ocpi.examples', 'components', 'include'))
    import ocpi_message
    from cq15 import cq15
    from q15 import q15
    import opcode_testing
    import ocpi_message as om

except Exception as e:
    sys.exit(e)

if __name__ == '__main__':

    input_messages = om.from_file(sys.argv[-1], 'complex', messages_in_file=True)
    worker_messages = om.from_file(sys.argv[-2], 'real', messages_in_file=True)
    
    for test, worker in zip(input_messages, worker_messages):
    
        if (test.get_opcode() != 'sample' and worker.get_opcode() != 'sample'):
            if (test != worker):
                sys.exit("Metadata ops should pass thorugh the worker.")
        
        # This isn't a normal magnitude computation. The result is downshifted by 16 bits
        # to normalize the output.
        elif (test.get_opcode() == 'sample' and worker.get_opcode() == 'sample'):
            for t, w in zip(test.get_data(), worker.get_data()):
                mag_sq = int(((float(t.real)*32767)**2 + (float(t.imag)*32767)**2)) >> 16
                if (not q15.almost_equal(q15(w), q15(mag_sq/32767))):
                    sys.exit("Data: " + str(t) + "\nWorker-computed Mag Sq: " + str(w) + "\nTest-computed Mag Sq: " +
                            q15(mag_sq/32767)) 
        
        else:
            sys.exit("Message opcode mismatch.")
