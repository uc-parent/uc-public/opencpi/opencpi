#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import numpy as np
import random
try:
    sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', 'ocpi.examples', 'components', 'include'))
    from cq15 import cq15
    import ocpi_message as om
except Exception as e:
    sys.exit(e)

'''
@brief: Wraps a list of data with other metadata opcodes in an order that'll be likely to break things, unless the
        worker is truly handling op cases.
@param: `data_arr`, a list of CQ15 values.
@return: a LIST of ocpi_message objects.  
'''
def wrap_in_metadata(data_arr):
    time = om.ocpi_message(protocol='complex', opcode='time')
    time['fraction'] = 0xCDAB
    time['seconds'] = 0x98EF
    flush = om.ocpi_message(protocol='complex', opcode='time')
    samp = om.ocpi_message(protocol='complex', opcode='sample')
    
    for i in data_arr:
        samp.append(i) # Expected to be CQ15 values. 

    return [time, time, samp, samp, time, flush, samp, flush, flush, samp]

if __name__ == '__main__':
    
    messages = []

    # Ensure edge cases are tested.
    data = [cq15(-32768/32767, -32768/32767), cq15(32767/32767, 32767/32767), cq15(61/32767, -254/32767)]
    messages += wrap_in_metadata(data)
    
    # Add some randomized data.
    N = 100 # how much data to add to the sample message (must be less than 1024).
    M = 10 # how many different samps messages to create.

    for m in range(M):
        random.seed(1)
        data = [cq15(float(random.uniform(-1, 1)), float(random.uniform(-1, 1))) for i in range(N)]
        messages += wrap_in_metadata(data)
    
    om.to_file(sys.argv[-1], messages, messages_in_file=True)
