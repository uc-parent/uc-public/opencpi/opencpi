.. build_packet_uc documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _build_packet_uc:


Simple Packet Builder (``build_packet_uc``)
===========================================
Takes in data read from file and encapsulates with header and tail bytes.

Design
------

A block diagram representation of the component function is given in :numref:`build_packet_uc-diagram`.

.. _build_packet_uc-diagram:

.. figure:: build_packet_uc.svg
   :alt: Skeleton alternative text.
   :align: center

   Simple packet structure.

At the start of application, a stream of synchronization bytes (0x55) are passed to the output port. These bytes are intended to stabalize the receiver demodulator. The required number of bytes is set by the 'num_of_sync_bytes' parameter.  It may be necessary to vary this number dependent of the desired rate. As the rate increases, more synchronization bytes may be required.

The header bytes are then passed to the output port. Both the header and tail bytes are sized to match the pattern specified by the application's receiver pattern detector component ``pattern_detector_b``. To support the FSK application, these values must match the values define for each pattern detection value.

Then, as unsigned character data bytes are received on the input port, the component forwards the bytes to the output. When an End-of-File (EOF) signal is received, the component passes the tail bytes to the output port and declares the application ‘done’.

The ``build_packet_uc`` component is intended to interface with the ``file_read`` component found in projects/core.  It does not support ``read_file``  with embedded messages.  The ``read_file`` property ‘messagesInFile’ must be set to ‘false’.

Interface
---------
.. literalinclude:: ../specs/build_packet_uc-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
The component does not support processing of OpCodes.  It is intended to be a simple component to receive only sample data on its input port. Specifically, it is intended to be used with the ``file_read`` component.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../build_packet_uc.rcc 

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

Limitations
-----------
Limitations of ``build_packet_uc`` are:

 * Input interface requires uchar stream with EOF to terminate.
 * The messagesInFile is not supported by this component.

Testing
-------
.. ocpi_documentation_test_platforms::

.. Removed ocpi_documentation_test_result_summary directive until it is functional
