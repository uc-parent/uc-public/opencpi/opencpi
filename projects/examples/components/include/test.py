#!/usr/bin/env python3

import unittest
from q15 import q15
from cq15 import cq15

class TestQ15Methods(unittest.TestCase):

    def test_init(self):

        self.assertEqual(q15(0.0), q15(0))
        self.assertEqual(q15(0), q15(0.0))

        self.assertEqual(q15(b'\x7f\xff'), q15(1.0))
        self.assertEqual(q15(b'\x80\x00'), q15(-1.0))

        q15(q15(0.23))

    # Test asserts first, because a lot of unit tests depend on them
    def test_assert_almost_equal(self):

        self.assertTrue(q15.assert_almost_equal(q15(0.230001), q15(0.23009)))
        self.assertTrue(q15.assert_almost_equal(q15(0.230001), q15(0.23009)))
        self.assertTrue(q15.assert_almost_equal(q15(0.230001), q15(0.23009)))
        with self.assertRaises(AssertionError):
            self.assertFalse(q15.assert_almost_equal(q15(0.230001), q15(0.23009), 4))

    def test_abs(self):

        self.assertEqual(q15(0.45), abs(q15(-0.45)))
        self.assertEqual(q15(0.0), abs(q15(0.0)))
        self.assertEqual(q15(0.98), abs(q15(-0.98)))
        self.assertNotEqual(q15(1.0), abs(q15(-1.0)))

    def test_add(self):

        self.assertEqual(q15(0.0) + q15(0.0), q15(0.0))
        self.assertTrue(q15.assert_almost_equal(q15(0.12) + q15(0.33), q15(0.449)))
        self.assertEqual(q15(-0.12) + q15(0.33), q15(0.21))
        self.assertEqual(q15(0.12) + q15(-0.33), q15(-0.21))
        self.assertEqual(q15(0.75) + q15(0.75), q15(1.0))
        self.assertEqual(q15(-0.75) + q15(-0.75), q15(-1.0))

    def test_and(self):

        self.assertEqual(q15(b'\x55\x55') & q15(b'\xaa\xaa'), q15(b'\x00\x00'))
        self.assertEqual(q15(b'\x55\x55') & q15(b'\x55\x55'), q15(b'\x55\x55'))
        self.assertEqual(q15(b'\xaa\x55') & q15(b'\x55\x55'), q15(b'\x00\x55'))

    def test_bytes(self):

        self.assertEqual(bytes(q15(-1.0)), b'\x80\x00')
        self.assertEqual(bytes(q15(1.0)), b'\x7f\xff')

    def test_eq(self):

        self.assertTrue(b'\x7f\xff' == q15(1.0))
        self.assertTrue(q15(1) == q15(1.0))
        self.assertTrue(q15(0.25) == b'\x20\x00')

    def test_float(self):

        self.assertTrue(q15.assert_almost_equal(float(q15(0.33)), 0.33))
        self.assertEqual(float(q15(0)), 0.0)
        self.assertTrue(q15.assert_almost_equal(float(q15(-0.92)), -0.92))

    def test_ge(self):

        self.assertTrue(q15(0.12) >= 0.12)
        self.assertTrue(q15(0.12) >= q15(0.11))
        self.assertTrue(q15(-0.55) >= q15(-0.84))

    def test_gt(self):

        self.assertFalse(q15(0.12) > 0.12)
        self.assertTrue(q15(0.44) > q15(0.0))
        self.assertTrue(q15(0.77) > 0.31)
        self.assertTrue(q15(-0.33) > q15(-0.88))

    def test_invert(self):

        self.assertEqual(~q15(b'\x55\x55'), q15(b'\xaa\xaa'))
        self.assertEqual(~q15(-1.0), q15(1.0))

    def test_le(self):

        self.assertFalse(q15(0.55) <= (0.12))
        self.assertTrue(q15(0.33) <= 0.33)
        self.assertTrue(q15(-0.22) <= 0.0)
        self.assertTrue(q15(-1.0) <= q15(1.0))

    def test_lshift(self):
        
        self.assertEqual(q15(b'\x55\x55') << 1, q15(b'\xaa\xaa'))
        self.assertEqual(q15(b'\xaa\xaa') << 1, b'\x55\x54')

    def test_lt(self):

        self.assertFalse(q15(0.44) < q15(0.33))
        self.assertTrue(q15(0.34) < q15(0.55))
        self.assertTrue(q15(0.33) < 0.54)
        self.assertTrue(q15(-0.88) < -0.12)
        self.assertFalse(q15(0.0) < 0.0)

    def test_mul(self):

        self.assertTrue(q15.assert_almost_equal(q15(1.0) * q15(1.0), 0.9999 * 0.9999))
        self.assertTrue(q15.assert_almost_equal(q15(0.23) * q15(0.56), 0.23 * 0.56))
        self.assertTrue(q15(0.0) * q15(0.34), q15(0.0))
        self.assertTrue(q15.assert_almost_equal(q15(-0.12) * 0.56, -0.12 * 0.56))
    
    def test_neg(self):

        self.assertEqual(-q15(0.98), q15(-0.98))
        self.assertEqual(-q15(0.0), q15(0.0))
        self.assertTrue(q15.assert_almost_equal(-q15(0.23), -0.23))

    def test_neq(self):

        self.assertTrue(q15(0.12) != q15(0.44))
        self.assertTrue(q15(0.0) != 0.1)
        self.assertTrue(q15(-0.25) != q15(0.25))
        self.assertFalse(q15(0.44) != q15(0.44))

    def test_or(self):

        self.assertEqual(q15(b'\x55\x55') | q15(b'\xaa\xaa'), q15(b'\xff\xff'))
        self.assertEqual(q15(b'\x00\x00') | q15(b'\x12\x34'), q15(b'\x12\x34'))
        self.assertEqual(q15(1.0) | q15(-1.0), q15(b'\xff\xff'))

    def test_pos(self):

        self.assertEqual(+q15(0.23), q15(0.23))
        self.assertEqual(+q15(0.0), q15(0.0))
        self.assertEqual(+q15(-0.23), q15(-0.23))

    def test_pow(self):

        self.assertTrue(q15(0.23) ** 2, q15(0.23) * q15(0.23))
        self.assertTrue(q15(0.55) ** q15(0.33), 0.55 ** 0.33)

    def test_radd(self):

        self.assertEqual(0.23 + q15(0.56), q15(0.23) + q15(0.56))
        self.assertEqual(0 + q15(0.33), q15(0.0) + q15(0.33))
        self.assertEqual(b'\x20\x00' + q15(0.25), q15(0.5))

    def test_rmul(self):

        self.assertEqual(0.34 * q15(0.55), q15(0.34) * q15(0.55))
        self.assertEqual(1 * q15(0.22), q15(1.0) * q15(0.22))
    
    def test_rshift(self):

        self.assertEqual(q15(b'\xaa\xaa') >> 1, q15(b'\xd5\x55'))
        self.assertEqual(q15(b'\x20\x00') >> 1, q15(b'\x10\x00'))

    def test_rsub(self):

        self.assertEqual(0.23 - q15(0.33), q15(0.23) - q15(0.33))
        self.assertEqual(0 - q15(0.55), q15(0.0) - q15(0.55))

    def test_rtruediv(self):

        self.assertEqual(0.55 / q15(0.12), q15(0.55) / q15(0.12))
        self.assertEqual(1 / q15(0.55), q15(1.0) / q15(0.55))

    def test_sub(self):

        self.assertEqual(q15(0.50) - 0.25, q15(0.25))
        self.assertTrue(q15.assert_almost_equal(q15(0.77) - 0.88, q15(0.77-0.88)))

    def test_truediv(self):

        self.assertEqual(q15(0.123) / 0.11, q15(0.123 / 0.11))
        with self.assertRaises(Exception):
            q15(0.55) / 0.0
        self.assertTrue(q15.assert_almost_equal(q15(-0.12) / 0.44, q15(-0.12 / 0.44)))

    def test_xor(self):

        self.assertEqual(q15(b'\xaa\xaa') ^ q15(b'\x55\x55'), q15(b'\xff\xff'))
        self.assertEqual(q15(b'\xff\x00') ^ q15(b'\xf0\xf0'), q15(b'\x0f\xf0'))

    def test_from_bytes(self):

        pass


class TestCQ15Methods(unittest.TestCase):

    def test_init(self):

        c = cq15(q15(0.25), q15(0.25))

        self.assertEqual(cq15(0.25, 0.25), c)
        self.assertEqual(cq15(b'\x20\x00', b'\x20\x00'), c)
        self.assertEqual(cq15(complex(0.25, 0.25)), c)

        c = cq15(q15(0.25))

        self.assertEqual(cq15(0.25, 0.0), c)
        self.assertEqual(cq15(b'\x20\x00\x00\x00'), c)
        self.assertEqual(complex(0.25, 0.0), c)


        self.assertEqual(q15(0.0), q15(0))

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()
