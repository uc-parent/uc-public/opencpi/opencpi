#!/usr/bin/env python3

''' q15.py: This python module simulates a q15 number in memory. It is designed to make
            unit testing easier for OpenCPI. '''

__author__  = 'Jared Kick'
__email__ = 'jared.kick@centauricorp.com'

import numpy as np
import sys

class q15:

    # Value of q15 number
    value = 0

    def __init__(self, val=0.0):

        # Initialize data members
        self.value = 0

        # Convert to float for less logic
        if type(val) is int:
            val = float(val)

        if type(val) is q15:
            self.value = val.value     # Yikes
        
        # If val is of type float, convert to q15 accordingly
        elif type(val) is float:

            # Move all the bits we care about to the left of the decimal
            val *= 2**15

            # Trim off insignificant bits
            val = round(val)

            # Saturate to 15 bits
            self.value = self.saturate(val)

        # Read from straight binary
        elif type(val) is bytes:

            # Make sure we only received two bytes
            if len(val) != 2:
                raise Exception('Bro, two bytes at a time.')

            # Convert to unsigned int
            val = int.from_bytes(val, 'big', signed=True)

            # Convert to q15 using previous logic
            self.value = val

        # If val is any other type, raise exception
        else:
            print("Type given:", type(val))
            raise Exception('Q15 can only be initialized with type \'q15\', \'float\', \'int\', or \'bytes\'.')


    def __abs__(self):

        # Initialize another q15 with the absolute value
        q = q15(0)
        q.value = abs(self.value)
        return q


    def __add__(self, other):

        # If other is not q15, try and create it
        if type(other) is not q15:
            other = q15(other)

        # Return q15 object initialized to summation
        q = q15(0)
        q.value = self.saturate(self.value + other.value)
        return q


    def __and__(self, other):

        other = q15(other)

        q = q15(0)
        q.value = int(np.bitwise_and(self.value, other.value, dtype=np.int16))
        return q


    def __bytes__(self):

        # Value will always be between -32767 and 32767
        return self.to_bytes(byteorder='big')


    def __eq__(self, other):

        # Make sure 'other' is q15
        if type(other) is not q15:
            other = q15(other)

        # Test value
        return self.value == other.value


    def __float__(self):

        # Divide by max q15 value
        return self.value / 2**15


    def __format__(self, format_spec):

        return format(float(self), format_spec)


    def __ge__(self, other):

        # If other is not q15, try to create it
        if type(other) is not q15:
            other = q15(other)

        # Compare and return
        return self.value >= other.value


    def __gt__(self, other):

        # If other is not q15, try to create it
        if type(other) is not q15:
            other = q15(other)

        # Compare and return
        return self.value > other.value


    def __index__(self):

        # Convert object to int
        return self.value


    def __invert__(self):

        # Initialize new object with all values flipped
        q = q15(0)
        q.value = int(np.invert(self.value, dtype=np.int16))
        return q


    def __le__(self, other):

        # If other is not q15, try to create it
        if type(other) is not q15:
            other = q15(other)

        # Compare and return
        return self.value <= other.value

    
    def __lshift__(self, other):

        assert(type(other) == int)
        assert(other >= 0)
        
        q = q15(0)
        q.value = int(np.left_shift(self.value, other, dtype=np.int16))
        return q


    def __lt__(self, other):

        # If other is not q15, try to create it
        if type(other) is not q15:
            other = q15(other)

        # Compare and return
        return self.value < other.value


    def __mod__(self, other):

        other = q15(other)

        q = q15(0)
        q.value = self.value % other.value
        return q


    def __mul__(self, other):

        # If other is not q15, try and create it
        if type(other) is not q15:
            other = q15(other)

        # Multiply values directly
        product = self.value * other.value

        # Round up
        product += 16384

        # Shift right 15 and write to object
        product >>= 15

        # Return new initialized q15 object
        q = q15(0)
        q.value = self.saturate(int(product))
        return q


    def __neg__(self):

        # Initialize new object with self negated
        q = q15(0)
        q.value = -self.value
        return q


    def __neq__(self, other):

        # If other is not q15, try to create it
        if type(other) is not q15:
            other = q15(other)

        # Return not equal
        return not self.__eq__(other)


    def __or__(self, other):

        other = q15(other)
        
        q = q15(0)
        q.value = int(np.bitwise_or(self.value, other.value, dtype=np.int16))
        return q


    def __pos__(self):

        # Initialize new object with same values
        q = q15(0)
        q.value = +self.value
        return q


    # Due to lack of a better option, this method operates on floats
    def __pow__(self, val):

        return q15(float(self) ** float(val))


    def __radd__(self, other):

        # Try to convert other to q15, then add
        return q15(other) + self


    def __repr__(self):

        # Convert to string and return
        return self.__str__()


    def __rmul__(self, other):

        # Try to convert other to q15, then multiply
        return q15(other) * self

    def __rshift__(self, other):    
        
        assert(type(other) == int)
        assert(other >= 0)
        
        q = q15(0)
        q.value = int(np.right_shift(self.value, other, dtype=np.int16))
        return q


    def __rsub__(self, other):

        # Try to convert other to q15, then subtract
        return q15(other) - self


    def __rtruediv__(self, other):

        # Try to convert other to q15, then divide
        return q15(other) / self


    def __str__(self):

        # Convert to float, to string, and return
        return str(self.__float__())


    def __sub__(self, other):

        # If other is not q15, try to create it
        if type(other) is not q15:
            other = q15(other)

        # Return q15 object initialized to difference
        q = q15(0)
        q.value = self.saturate(self.value - other.value)
        return q


    def __truediv__(self, other):

        # If other is not q15, try to create it
        if type(other) is not q15:
            other = q15(other)

        # Insult the user's intelligence
        if other.value == 0:
            raise Exception('Division by zero.')

        # Divide
        quotient = self.saturate(int((self.value << 15) / other.value))

        # Initialize new q15 object and return
        q = q15(0)
        q.value = quotient
        return q


    def __xor__(self, other):

        other = q15(other)
        
        q = q15(0)
        q.value = int(np.bitwise_xor(self.value, other.value, dtype=np.int16))
        return q


    # Returns True if both given q15 numbers are close to being equal, False if not
    def almost_equal(actual, desired, precision=0.05):

        try:
            q15.assert_almost_equal(actual, desired, precision)
            return True

        except AssertionError:
            return False


    # Raises AssertionError if both given q15 numbers are not close to being equal
    def assert_almost_equal(actual, desired, precision=0.05):

        diff = abs(actual - desired)

        if diff > precision:
            raise AssertionError("%s != %s" % (actual, desired))
        else:
            return True


    def from_bytes(self, b, byteorder='big'):

        # Make sure we only received two bytes
        if len(b) != 2:
            raise Exception('Bro, two bytes at a time.')

        # Convert to unsigned int
        self.value = int.from_bytes(b, byteorder, signed=True)

        return self

    def to_integer(self):
        return self.value
        
    def saturate(self, num):

        num_bits = 15

        # Make sure value is within possible q15 values
        if num < -(2**num_bits):
            return -(2**num_bits)

        elif num > (2**num_bits - 1):
            return (2**num_bits - 1)

        else:
            return num


    def to_bytes(self, byteorder='big'):

        # Value will always be between -32767 and 32767
        return self.value.to_bytes(2, byteorder, signed=True)
