#!/usr/bin/env python3

''' cq15.py: This python module simulates a complex q15 number in memory. It is
             designed to make unit testing easier for OpenCPI. '''

__author__  = 'Jared Kick'
__email__ = 'jared.kick@centauricorp.com'

import sys
from q15 import q15


class cq15:

    # Real part of number
    real = None

    # Imaginary part of number
    imag = None

    def __init__(self, r, i=None):

        # If r is defined, but i is not
        if r is not None and i is None:

            if type(r) is bytes:

                if len(r) != 4:
                    raise Exception('cq15 requires 4 bytes total to initialize.')

                self.real = q15(0).from_bytes(r[0:2], 'big')
                self.imag = q15(0).from_bytes(r[2:4], 'big')

                return

            if type(r) is q15:
            
                self.real = r
                self.imag = q15(0.0)

            if type(r) is cq15:

                self.real = r.real
                self.imag = r.imag

                return

            elif type(r) is complex:

                self.real = q15(r.real)
                self.imag = q15(r.imag)

                return

        # Make sure it is of type q15
        elif type(r) is q15:
            self.real = r
        # Otherwise, try to convert it
        else:
            self.real = q15(r)

        # Initialize imaginary part
        if i:
            # Make sure it is of type q15
            if type(i) is q15:
                self.imag = i
            # Otherwise, try to convert it
            else:
                self.imag = q15(i)
        # Initialize to zero
        else:
            self.imag = q15(0)


    def __abs__(self):

        raise Exception('This method has not yet been implemented. First sqrt() \
                         must be defined for q15.')


    def __add__(self, other):

        # Make sure other is complex
        if type(other) is not cq15:
            other = cq15(other)

        # Add each cq15 object and return
        return cq15(self.real + other.real, self.imag + other.imag)


    def __bytes__(self):

        # Convert each part to bytes and concatenate
        return bytes(self.real) + bytes(self.imag)


    def __complex__(self):

        # Convert each part to floats and initialize to complex
        return complex(float(self.real), float(self.imag))


    def __eq__(self, other):

        # Make sure 'other' is cq15
        if type(other) is not cq15:
            other = cq15(other)

        # Test each value
        return self.real == other.real and self.imag == other.imag


    def __float__(self):

        # Not a good idea
        raise Exception('Cannot convert cq15 to float. Use cq15.real or cq15.imag instead.')


    def __index__(self):

        raise Exception('__index__() not implemented yet.')


    def __invert__(self):

        raise Exception('__invert__() not implemented yet.')


    def __mul__(self, other):

        # Use the left distributive law
        real = self.real * other.real - self.imag * other.imag
        imag = self.real * other.imag + other.real * self.imag

        # Return new object
        return cq15(real, imag)


    def __neg__(self):

        # Multiply by -1
        return cq15(-self.real, -self.imag)


    def __pos__(self):

        # Initialize new object with same values
        return cq15(self.real, self.imag)


    def __pow__(self, other):

        # Multiply repeatedly
        for i in range(other):
            self.__mul__(self)


    def __radd__(self, other):

        # Try to convert other to cq15, then add
        return cq15(other) + self


    def __repr__(self):

        return self.__str__()


    def __rmul__(self, other):

        # Try to convert other to cq15, then multiply
        return cq15(other) * self


    def __rsub__(self, other):

        # Try to convert other to cq15, then subtract
        return cq15(other) - self


    def __rtruediv__(self, other):

        # Try to convert other to cq15, then divide
        return cq15(other) / self


    def __str__(self):

        # Use same format as built-in python complex type
        string = '(' + str(self.real)
        if float(self.imag) >= 0.0:
            string += '+'
        # Negative sign is automatic

        # Return entire number
        return string + str(self.imag) + 'j)'


    def __sub__(self, other):

        # Forget it, just multiply other by -1 and add
        if type(other) is not cq15:
            other = cq15(other)

        return self + -other


    def __truediv__(self, other):

        # Make sure other is cq15
        if type(other) is not cq15:
            other = cq15(other)

        # Do q15 math manually to avoid saturation issues
        ar = self.real.value
        ai = self.imag.value
        br = other.real.value
        bi = other.imag.value

        # Get conjugate of divisor
        cr = br
        ci = -bi

        # Quick and dirty q15 multiply without saturation
        def mul(a, b):

            # Multiply
            product = a * b

            # Round up
            product += 16384

            # Shift right
            product /= 2**15

            # Return
            return int(product)

        # Get numerator
        nr = mul(ar, cr) - mul(ai, ci)
        ni = mul(ar, ci) + mul(ai, cr)

        # Get denominator (will always be real)
        dr = mul(br, cr) - mul(bi, ci)

        # Divide both parts of numerator by denominator
        return cq15(nr / dr, ni / dr)


    # Returns True if both given cq15 numbers are close to being equal, False if not
    def almost_equal(actual, desired, precision=0.05):

        try:
            cq15.assert_almost_equal(actual, desired, precision=precision)
            return True

        except AssertionError:
            return False


    # Raises AssertionError if both given q15 numbers are not close to being equal
    def assert_almost_equal(actual, desired, precision=0.05):

        actual = cq15(actual)
        desired = cq15(desired)

        assert(q15.assert_almost_equal(actual.real, desired.real, precision=precision))
        assert(q15.assert_almost_equal(actual.imag, desired.imag, precision=precision))


    def from_bytes(self, b, byteorder='big'):

        if len(b) != 4:
            raise Exception('cq15 requires total of 4 bytes to initalize.')

        self.real = q15(0).from_bytes(b[0:2], byteorder)
        self.imag = q15(0).from_bytes(b[2:4], byteorder)

        return self


    def to_bytes(self, byteorder='big'):

        return self.real.to_bytes(byteorder) + self.imag.to_bytes(byteorder)
