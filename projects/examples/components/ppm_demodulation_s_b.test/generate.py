#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os

from numpy.lib.function_base import _meshgrid_dispatcher
sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', \
    'ocpi.examples', 'components', 'include'))
from q15 import q15
import ocpi_message as om

# Get properties from the test runner's XML.
try:
    sps=int(os.environ.get("OCPI_TEST_samps_per_sym"))
    leading_zero=(os.environ.get("OCPI_TEST_one_has_leading_zero"))
    # Handle the Boolean "casting"
    if ("false" in leading_zero.lower()):
        leading_zero = False
    else:
        leading_zero = True
except Exception as e:
    print("Could not get environment variables.\nError:  " + str(e))
    sys.exit(1)

# Constants
REAL_SHORT_MAX = 2**16-1 # real_short (input type), a 16 bit val
REAL_SHORT_MIN = 0
HALF_SPS = int(sps/2)

if leading_zero == False:
    one = [REAL_SHORT_MAX] * HALF_SPS + [REAL_SHORT_MIN] * HALF_SPS
    zero = [REAL_SHORT_MIN] * HALF_SPS + [REAL_SHORT_MAX] * HALF_SPS
else:
    zero = [REAL_SHORT_MAX] * HALF_SPS + [REAL_SHORT_MIN] * HALF_SPS
    one = [REAL_SHORT_MIN] * HALF_SPS + [REAL_SHORT_MAX] * HALF_SPS


src_data = one * 3 + zero * 3 + one * 3 + zero * 3
# src_data = [REAL_SHORT_MAX] * 3 + [REAL_SHORT_MIN] * 3 + \
#              [REAL_SHORT_MAX] * 3 + [REAL_SHORT_MIN] * 3

'''
Sequence of opcodes that covers all messages, including zero length
message transitions.
Time (T), Sample(S), Flush (F)
T > T > S > S > T > F > S > F > F > S

Write these to the output file.

Note: Zero-length sample messages are commented out here. They should not be
      sent to this worker.
'''
if __name__ == '__main__':

    messages = []

    # Data messages
    m = om.ocpi_message(protocol='real')
    for val in src_data:
        m.append(q15(val))
    messages = [m]
    
    # Two 'time' ops: T T
    #m = om.ocpi_message(protocol='real', opcode='time')
    #messages += [m, m]
    
    # Two zero-length 'sample' ops: S S
    #m = om.ocpi_message(protocol='real', opcode='sample')
    #messages += [m]

    # One 'time' op: T 
    #m = om.ocpi_message(protocol='real', opcode='time')
    #messages += [m]

    # One 'flush' op: F
    #m = om.ocpi_message(protocol='real', opcode='flush')
    #messages += [m]
    
    # Data messages
    #m = om.ocpi_message(protocol='real')
    #for val in src_data:
    #    m.append(q15(val))
    #messages += [m]
    
    # One ZLM 'sample' op: S
    #m = om.ocpi_message(protocol='real', opcode='sample')
    #messages += [m]

    # Data message
    #m = om.ocpi_message(protocol='real')
    #for val in src_data:
    #    m.append(q15(val))
    #messages += [m]

    # Two 'flush' ops: F F
    #m = om.ocpi_message(protocol='real', opcode='flush')
    #messages += [m, m]

    # One ZLM 'sample' op: S
    #m = om.ocpi_message(protocol='real', opcode='sample')
    #messages += [m]
    
    # Dump all the accumulated messages to a file.
    print(sys.argv[-1])
    om.to_file(sys.argv[-1], messages, messages_in_file=True)

